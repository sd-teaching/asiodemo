#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

// tcp::socket &global_socket;


class httpClient {
private:
    std::string host_name;
    std::string port_name = "80";

    tcp::socket socket;
    tcp::resolver resolver;

    int buf_size = 256;
    std::vector<char> buffer;

    int chunkNum = 0;
    std::string result;

    std::shared_ptr<boost::asio::io_context> context_ptr;

public:

    httpClient(std::shared_ptr<boost::asio::io_context> context_ptr, std::string host) : context_ptr(context_ptr), resolver(*context_ptr), socket(*context_ptr), buffer(buf_size), host_name(std::move(host)) {
    }

    void start() {
        tcp::resolver::results_type endpoints = resolver.resolve(host_name, "80");

        boost::asio::async_connect(socket, endpoints, std::bind(&httpClient::connectHandler, this, std::placeholders::_1, std::placeholders::_2));
    }

    void connectHandler(const boost::system::error_code &error, const tcp::endpoint &endpoint) {
        if(!error) {
            std::cout << std::this_thread::get_id() << " " << host_name << ": " << "Connected to endpoint " << endpoint << std::endl;

            std::string request(std::string("GET / HTTP/1.0\nHost: ") + host_name + std::string("\n\n"));

            boost::asio::async_write(socket, boost::asio::buffer(request), std::bind(&httpClient::writeHandler, this, std::placeholders::_1, std::placeholders::_2));
        }
        else {
            std::cout << std::this_thread::get_id() << " " << host_name << ": " << "Error connecting to endpoint " << endpoint << "; " << error.message() << std::endl;
        }
    }

    void writeHandler(const boost::system::error_code &error, size_t bytesWritten) {
        if(!error) {
            std::cout << std::this_thread::get_id() << " " << host_name << ": " << "Data sent - written " << bytesWritten << " bytes" << std::endl;

            chunkNum = 0;
            readNextChunk();
        }
        else {
            std::cout << std::this_thread::get_id() << " " << host_name << ": " << "Error writing bytes " << error.message() << std::endl;
        }
    }

    void readNextChunk() {
        buffer.assign(buf_size, 0);
        boost::asio::async_read(socket, boost::asio::buffer(buffer, buf_size-1), std::bind(&httpClient::readHandler, this, std::placeholders::_1));
    }

    void readHandler(const boost::system::error_code &error) {
        if(!error) {
            result += buffer.data();

            {
                using namespace std::chrono_literals;
                std::this_thread::sleep_for(1ms);
            }

            std::cout << std::this_thread::get_id() << " " << host_name << ": " << "next chunk read - " << strlen(buffer.data()) << std::endl;

            chunkNum++;
            readNextChunk();
        }
        else {
            if(error.value() == boost::asio::error::eof) {
                result += buffer.data();
                std::cout << std::this_thread::get_id() << " " << host_name << ": " << "Read finished. Read bytes: " << result.length() << std::endl;
            }
            else {
                std::cout << std::this_thread::get_id() << " " << host_name << ": " << "Error reading data: " << error.message() << std::endl;
            }

            socket.close();
        }
    }

    std::string getResult() {
        return result;
    }

    std::string getHost() {
        return host_name;
    }
};



int main() {
    std::cout << "Hello, Asio!" << std::endl;

    try
    {
        std::shared_ptr<boost::asio::io_context> context_ptr(new boost::asio::io_context);

        httpClient client1(context_ptr, "vhfztkmxwlrbcdns.neverssl.com");
        httpClient client2(context_ptr, "www.knu.ua");

        client1.start();
        client2.start();

        std::thread thread1{[context_ptr](){ context_ptr->run(); }};
        std::thread thread2{[context_ptr](){ context_ptr->run(); }};

        thread1.join();
        thread2.join();

        std::cout << client1.getHost() << std::endl << client1.getResult() << std::endl;
        std::cout << client2.getHost() << std::endl << client2.getResult() << std::endl;
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    std::cout << "Program finished." << std::endl;

    return 0;
}